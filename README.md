# Jazz Contribute

**If you want to  be a contributor, [write to us](mailto:jazzcoreteam@gmail.com)**

> This repository is to collect issues from the community, suggestions for roadmap and also to consolidate an awesome list of jazz-packs and jazz-plugins currently available.

## Awesome List

#### Jazz-Core

The main project, available only for Jazz-Core team contributors. 

#### Jazz-Packs: 

To install the packs below, use:

```bash 
 npm install {packname}
```
   
* <a href="https://www.npmjs.com/package/jazz-pack-citibank-paylink" target="_blank">jazz-pack-citibank-paylink</a> 


#### Jazz-Plugins:

* <a href="https://www.npmjs.com/package/jazz-plugin-excelextractor" target="_blank">jazz-plugin-excelextractor</a> : Extracts data from excel spreadsheet to a javascript array
* <a href="https://www.npmjs.com/package/jazz-plugin-textloader" target="_blank">jazz-plugin-textloader</a>: Converts a javascript array into a text file



## Roadmap

> This section will describe only the main functionalities in the product roadmap. 
If you want something to your use case that is still not available, please open an issue and we will go for it!

### Jazz-Core
* Develop the Jazz Task Runner User Interface
* Develop the Jazz MarketPlace platform
* Develop The Jazz Composer User Interface

### Jazz-Plugins: 

#### jazz-plugin-excelextractor
* read excel with password

 

#### New plugins:

- RestAPI consumer
- Database reader (postgres, mssql, mongodb, mysql, oracle)
- sFTP / FTP connector
- CSV reader
- XML Reader
- GraphQL API Reader

